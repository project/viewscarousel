(function($) {
  var viewscarouselHTML = new Array();
  var uuid = 0;
  Drupal.behaviors.viewscarousel = function(context) {
    $.each(Drupal.settings.viewscarousel, function(id) {
      if (this.scroll) this.scroll = parseInt(this.scroll);
      if (this.start) this.start = parseInt(this.start);
      if (this.visible) this.visible = parseInt(this.visible);
      if (this.auto) this.auto = parseInt(this.auto);
      $this = $('#' + id);
      if (this.wrap == 'circular') {
        this.itemVisibleInCallback = {onBeforeAnimation: viewscarouselItemVisibleInCallback};
        this.itemVisibleOutCallback = {onAfterAnimation: viewscarouselItemVisibleOutCallback};
        this.viewscarouseluuid = uuid;
        viewscarouselHTML[uuid] = new Array();
        $.each($this.children(), function () {
          viewscarouselHTML[uuid].push($(this).html());
        });
        uuid++;
      }
      $this.jcarousel(this);
    });
  }

  function viewscarouselItemVisibleInCallback(carousel, item, i, state, evt) {
    var idx = carousel.index(i, viewscarouselHTML[carousel.options.viewscarouseluuid].length);
    carousel.add(i, viewscarouselHTML[carousel.options.viewscarouseluuid][idx - 1]);
  };

  function viewscarouselItemVisibleOutCallback(carousel, item, i, state, evt) {
    carousel.remove(i);
  };
})(jQuery)